defmodule PortBlock do
  use Application

  def start(_type, _args) do
    :cowboy.start_clear(:http, [port: 3000], protocolOpts())
  end

  defp protocolOpts do
    dispatch = :cowboy_router.compile routes()
    %{env: %{dispatch: dispatch}}
  end

  defp routes do
    [
      {:_, [
        {"/", PortBlock.Handlers.Hello, []}
      ]}
    ]
  end
end
