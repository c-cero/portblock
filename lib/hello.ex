defmodule PortBlock.Handlers.Hello do

  def init(req0, opts) do
    req = :cowboy_req.reply(200, %{<<"content-type">> => <<"text/plain">>}, <<"Hello, cowboy!">>, req0)
    {:ok, req, opts}
  end
end